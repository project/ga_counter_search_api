<?php
/**
 * @file
 * This module allows to index the Google Analytics Counter data using Search API.
 */

/**
 * Implements hook_entity_property_info_alter().
 */
function ga_counter_search_api_entity_property_info_alter(&$info) {
  $info['node']['properties']['ga_counter'] = array(
    'type' => 'integer',
    'label' => t('Google analytics counter'),
    'sanitized' => TRUE,
    'getter callback' => 'ga_counter_search_api_entity_property_counter_getter_callback',
  );
}

/**
 * Getter callback for ga_counter.
 */
function ga_counter_search_api_entity_property_counter_getter_callback($item) {
  // Get the number of pageviews for this node.
  $nid = $item->nid;
  $gac_count = db_select('google_analytics_counter_storage', 'gacs')
    ->fields('gacs', array('pageview_total'))
    ->condition('nid', $nid, '=')
    ->execute()
    ->fetchCol();
  return $gac_count;
}

/**
 * Implements hook_google_analytics_counter_update().
 *
 * Mark updated nodes as "dirty", i.e., as needing to be reindexed.
 */
function ga_counter_search_api_google_analytics_counter_update($updated_nids) {
  search_api_track_item_change('node', array_keys($updated_nids));
}
