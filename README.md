INTRODUCTION
------------

This modules bridges Google Analytics Counter and Search API. It adds a field 
which can then be indexed. When Google Analytics Counter updates the node count,
it will mark the updated item for reindexing.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/sneakyvv/2736505

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2736505

REQUIREMENTS
------------

This module requires the following modules:

 * Google Analytics Counter (https://drupal.org/project/google_analytics_counter)
 * Search API (https://drupal.org/project/search_api)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
   
CONFIGURATION
-------------

The module has no menu or modifiable settings. It will just hook into the 
hook_google_analytics_counter_update.

MAINTAINERS
-----------

Current maintainers:
 * Sneakyvv - https://drupal.org/user/1823074
